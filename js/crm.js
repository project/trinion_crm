(function ($, Drupal) {
  Drupal.behaviors.crm = {
    attach: function (context, settings) {
      $(".pole_kompanii").change(function (){
        changeQueryHash($(this), 'comp');
        $(".pole_kontakta").val('');
      });
      function changeQueryHash(el, name) {
        params = new Proxy(new URLSearchParams(window.location.search), {
          get: (searchParams, prop) => searchParams.get(prop),
        });
        var url = document.location.pathname;
        var inserted = false;
        ['org', 'comp'].forEach(function(element, index){
          prefix = index && inserted ? '&' : '?';
          if (element == name) {
            url += prefix + name + '=' + el.val();
            inserted = true;
          }
          else {
            if (params[element] != null) {
              url += prefix + name + '=' + params[element];
              inserted = true;
            }
          }
        });
        window.history.pushState("", "", url);
      }
    }
  };
})(jQuery, Drupal);
