<?php

namespace Drupal\trinion_crm\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\Routing\Route;

/**
 * Проверка доступа к утверждению документа
 */
class UtverditDokumentChecker implements AccessInterface {

  /**
   * Access callback.
   */
  public function access(Route $route, $node, $op) {
    $node = Node::load($node);
    $bundle = $node->bundle();
    if ($node->hasField('field_tl_otvetstvennyy') &&
      $node->hasField('field_tl_utverzhdeno') &&
      (
        $node->get('field_tl_utverzhdeno')->getString() != $op
      )
    ) {
      $user = \Drupal::currentUser();
      if ($user->hasPermission('trinion_base edit all'))
        return AccessResult::allowed();
      if ($user->id() == $node->get('field_tl_otvetstvennyy')->getString())
        return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }
}
