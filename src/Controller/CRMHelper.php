<?php

namespace Drupal\trinion_crm\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CRMHelper extends ControllerBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(EntityTypeManager $entity_type_manager, Connection $connection) {
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  public function normalizePhone($str) {
    return preg_replace('/[^+0-9]/', '', $str);
  }

  /**
   * Получение Контакта или Компании по мылу или номеру телефона
   * @param null $mail
   * @param null $phone
   *
   * @return mixed
   */
  public function getContactOrCompany($mail = NULL, $phone = NULL) {
    $query = \Drupal::entityQuery('node')
      ->condition('type', ['contact', 'kompanii'], 'IN')
      ->condition('field_e_mail', $mail);
    $cont_comp = $query->execute();
    if (empty($cont_comp)) {
      if ($phone) {
        $query = \Drupal::entityQuery('node')
          ->condition('type', 'contact')
          ->condition('field_client_phone', $phone);
        $cont_comp = $query->execute();
        if (empty($cont_comp)) {
          $query = \Drupal::entityQuery('node')
            ->condition('type', 'kompanii')
            ->condition('field_nomer_telefona', $phone);
          $cont_comp = $query->execute();
        }
      }
    }
    return $cont_comp;
  }

  /**
   * Получение следующего номера документа
   */
  public function getNextDocumentNumber($type) {
    $start_nomer = 1;
    $query = $this->connection->select('node_field_data', 'n')
      ->condition('n.type', $type);
    $query->addField('n', 'title');
    $query->addExpression('CAST (n.title AS UNSIGNED)', 't');
    $query->orderBy('t', 'DESC');
    $query->range(0, 1);
    $res = $query->execute()->fetchField();
    return !$res || $res < $start_nomer ? $start_nomer : $res + 1;
  }
}
