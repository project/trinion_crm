<?php

namespace Drupal\trinion_crm\Controller;

use Dompdf\Dompdf;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * PDF компания
 */
class PDFKompaniya extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build(Node $node) {
    $build['content'] = [
      '#theme' => 'kompaniya_pdf',
      "#node" => $node,
      "#root_path" => \Drupal::service('file_system')->realpath(''),
    ];
    $html = \Drupal::service('renderer')->render($build);

    $dompdf = new Dompdf();
    $dompdf->loadHtml($html);
    $options = $dompdf->getOptions();
    $options->set('chroot', DRUPAL_ROOT);
    $dompdf->setOptions($options);

    $dompdf->render();

    $dompdf->stream("zakaz-kompaniya.pdf", ["Attachment" => false]);
    exit;
  }

}

