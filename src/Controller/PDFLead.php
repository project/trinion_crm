<?php

namespace Drupal\trinion_crm\Controller;

use Dompdf\Dompdf;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * PDF лид
 */
class PDFLead extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build(Node $node) {
    $build['content'] = [
      '#theme' => 'lead_pdf',
      "#node" => $node,
      "#root_path" => \Drupal::service('file_system')->realpath(''),
    ];
    $html = \Drupal::service('renderer')->render($build);

    $dompdf = new Dompdf();
    $dompdf->loadHtml($html);
    $options = $dompdf->getOptions();
    $options->set('chroot', DRUPAL_ROOT);
    $dompdf->setOptions($options);

    $dompdf->render();

    $dompdf->stream("lead.pdf", ["Attachment" => false]);
    exit;
  }

}

