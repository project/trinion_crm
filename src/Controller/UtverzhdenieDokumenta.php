<?php

namespace Drupal\trinion_crm\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * Утверждение или отмена документа
 */
class UtverzhdenieDokumenta extends ControllerBase {

  /**
   * Builds the response.
   * @param $op 0 - аннулирован, 1 - утвержден
   */
  public function build(Node $node, $op) {
    $response = new AjaxResponse();
    $bundle = $node->bundle();
    $transaction = \Drupal::database()->startTransaction();
    if ($bundle == 'sdelki') {
      $node->field_tl_utverzhdeno = $op;
    }
    $node->save();
    $response->addCommand(new RedirectCommand('/node/' . $node->id()));
    unset($transaction);
    return $response;
  }
}
