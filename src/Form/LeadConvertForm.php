<?php

namespace Drupal\trinion_crm\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Provides a Trinion lead form.
 */
class LeadConvertForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_crm_lead_convert';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $lead_id = \Drupal::request()->get('lead_id');
    $lead = Node::load($lead_id);
    if (empty($lead))
      return [];

    $form['create_company'] = [
      '#type' => 'checkbox',
      '#title' => 'Создать компанию',
      '#default_value' => TRUE,
    ];

    $form['company_name'] = [
      '#type' => 'textfield',
      '#title' => 'Название компании',
      '#states' => [
        'visible' => [
          ':input[name="create_company"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['contact_name'] = [
      '#type' => 'textfield',
      '#title' => 'Название контакта',
      '#required' => TRUE,
    ];

    $form['phone'] = [
      '#type' => 'textfield',
      '#title' => 'Телефон',
      '#default_value' => $phone = $lead->get('field_tl_nomer_telefona')->getString(),
    ];

    $form['email'] = [
      '#type' => 'email',
      '#title' => 'Email',
      '#default_value' => $phone = $lead->get('field_tl_email')->getString(),
    ];

    $form['lead_text'] = [
      '#type' => 'textarea',
      '#title' => 'Данные лида',
      '#value' => $lead->get('field_tl_text')->getString(),
      '#disabled' => TRUE,
    ];

    $form['lead_id'] = [
      '#type' => 'hidden',
      '#value' => $lead_id,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Создать',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('phone') == '' && $form_state->getValue('email') == '') {
      $form_state->setErrorByName('phone', 'Номер телефона или адрес электронной почты обязательны для заполения');
    }
    if ($form_state->getValue('create_company') && $form_state->getValue('company_name') == '') {
      $form_state->setErrorByName('phone', 'Укажите наименование компании');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $phone = $form_state->getValue('phone');
    $email = $form_state->getValue('email');
    $lead = Node::load($form_state->getValue('lead_id'));
    $contact_data = [
      'type' => 'contact',
      'title' => $form_state->getValue('contact_name'),
      'uid' => \Drupal::currentUser()->id(),
      'status' => 1,
    ];
    if ($form_state->getValue('create_company')) {
      $company_data = [
        'type' => 'kompanii',
        'title' => $form_state->getValue('company_name'),
        'uid' => \Drupal::currentUser()->id(),
        'status' => 1,
        'field_tl_tip_kompanii' => 'Customer',
      ];
      if ($phone != '')
        $company_data['field_tl_nomer_telefona'] = $phone;
      if ($email != '')
        $company_data['field_tl_email'] = $email;
      $company = Node::create($company_data);
      $company->save();
      \Drupal::messenger()->addMessage('Компания создана');
      $company_id = $company->id();
      $contact_data['field_tl_kompaniya'] = $company_id;
      $lead->set('field_tl_kompaniya', $company_id);
    }
    if ($phone != '')
      $contact_data['field_tl_nomer_telefona'] = $phone;
    if ($email != '')
      $contact_data['field_tl_email'] = $email;
    $contact = Node::create($contact_data);
    $contact->save();
    \Drupal::messenger()->addMessage('Контакт создан');
    $lead->set('field_tl_contact', $contact->id());
    $lead->save();
    $form_state->setRedirect('entity.node.canonical', ['node' => $contact->id()]);
  }
}
