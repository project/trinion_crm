<?php

namespace Drupal\trinion_crm\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Configure Trinion crm settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_crm_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['trinion_crm.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('trinion_crm.settings');
    $form['sdelki_start_nomer'] = [
      '#type' => 'number',
      '#title' => 'Цифра с которой начнется нумерация Сделок',
      '#default_value' => $config->get('sdelki_start_nomer'),
    ];

    $form['lid_poluchatel_uvedomleniya'] = [
      '#type' => 'entity_autocomplete',
      '#title' => 'Список пользователей, кому будет отправляться уведомление о новом лиде',
      '#target_type' => 'user',
      '#tags' => TRUE,
      '#description' => 'Через запятую',
      '#default_value' => [],
    ];
    if ($managers = $config->get('lid_poluchatel_uvedomleniya')) {
      foreach ($managers as $item) {
        if ($user = User::load($item['target_id']))
          $form['lid_poluchatel_uvedomleniya']['#default_value'][] = $user;
      }
    }

    foreach (\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('organizaciya', 0, 1, FALSE) as $term)
      $opts[$term->tid] = $term->name;
    $form['default_organization'] = [
      '#type' => 'select',
      '#title' => t('Default value for Organization field'),
      '#options' => $opts,
      '#default_value' => $config->get('default_organization'),
      '#empty_value' => ''
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('trinion_crm.settings')
      ->set('sdelki_start_nomer', $form_state->getValue('sdelki_start_nomer'))
      ->set('lid_poluchatel_uvedomleniya', $form_state->getValue('lid_poluchatel_uvedomleniya'))
      ->set('default_organization', $form_state->getValue('default_organization'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
