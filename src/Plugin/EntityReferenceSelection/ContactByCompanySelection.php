<?php

declare(strict_types=1);

namespace Drupal\trinion_crm\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\node\Plugin\EntityReferenceSelection\NodeSelection;

/**
 * @todo Add plugin description here.
 *
 * @EntityReferenceSelection(
 *   id = "trinion_crm_contact_by_company_selection",
 *   label = @Translation("Contact by company selection"),
 *   group = "trinion_crm_contact_by_company_selection",
 *   entity_types = {"node"},
 * )
 */
final class ContactByCompanySelection extends NodeSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS'): QueryInterface {
    $query = parent::buildEntityQuery($match, $match_operator);
    // @todo Modify the query here.
    return $query;
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return [
      '#markup' => t('Work only for Estimates, Sales orders, Purchase orders, Bills'),
    ];
  }

  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    if (($company = \Drupal::request()->get('field_tp_schet_dlya')) || $company = \Drupal::request()->get('field_tp_zakaz_dlya')) {
      if (preg_match_all('/\((\d+)\)("|)/', $company[0]['target_id'], $match, PREG_SET_ORDER)) {
        $match = array_pop($match);
        $company_id = $match[1];
      }
    }
    else {
      $route_match = \Drupal::routeMatch();
      $route_name = $route_match->getRouteName();
      if ($route_name == 'entity.node.edit_form') {
        $node = $route_match->getParameter('node');
        switch ($node->bundle()) {
          case 'kommercheskoe_predlozhenie':
          case 'schet_postavschika':
          case 'schet':
            if ($node->get('field_tp_schet_dlya')->getString() && $company = Node::load($node->get('field_tp_schet_dlya')->getString()))
              $company_id = $company->id();
            break;
          case 'zakaz_klienta':
          case 'zakaz_postavschiku':
            if ($node->get('field_tp_zakaz_dlya')->getString() && $company = Node::load($node->get('field_tp_zakaz_dlya')->getString()))
              $company_id = $company->id();
            break;
        }
      }
    }
    if (!empty($company_id)) {
      $query = \Drupal::entityQuery('node')
        ->condition('type', 'contact')
        ->condition('field_tl_kompaniya', $company_id);
      $res = $query->accessCheck()->execute();
      $response = [];
      if ($res) {
        foreach (Node::loadMultiple($res) as $contact)
          $response[$contact->id()] = $contact->label();
      }
      return ['contact' => $response];
    }
    return [];
  }
}
